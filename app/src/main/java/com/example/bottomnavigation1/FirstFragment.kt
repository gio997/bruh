package com.example.bottomnavigation1

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation

class FirstFragment : Fragment(R.layout.fragment_first) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    var pass : Button =findViewbyId(R.id.button)
    var inputedtext :EditText = findViewbyId(R.id.inputtext)

            val controller = Navigation.findNavController(view)
        pass.setOnClickListener {
            var text=inputedtext.text.toString()
            if(!text.isEmpty()){

            val action =FirstfragmentDirections.actionHomeFragmentToFragmentsecond(text)
                controller.navigate(action) }
        }
    } }